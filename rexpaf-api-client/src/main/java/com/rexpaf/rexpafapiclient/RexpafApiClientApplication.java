package com.rexpaf.rexpafapiclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RexpafApiClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(RexpafApiClientApplication.class, args);
    }

}
