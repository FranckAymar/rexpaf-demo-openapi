package com.rexpaf.rexpafapiclient;

import org.openapitools.client.ApiClient;
import org.openapitools.client.api.AdresseApi;
import org.openapitools.client.api.CotisationApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public CotisationApi cotisationApi(){
        return new CotisationApi(new ApiClient().setBasePath("http://localhost:8080"));
    }

    @Bean
    public AdresseApi adresseApi(){
        return new AdresseApi(new ApiClient().setBasePath("http://localhost:8081"));
    }
}

