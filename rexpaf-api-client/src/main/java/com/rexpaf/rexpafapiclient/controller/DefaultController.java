package com.rexpaf.rexpafapiclient.controller;

import com.rexpaf.rexpafapiclient.InfoMembreRexpaf;
import org.openapitools.client.api.AdresseApi;
import org.openapitools.client.api.CotisationApi;
import org.openapitools.client.model.Adresse;
import org.openapitools.client.model.Cotisation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {

    private final CotisationApi cotisationApi;
    private final AdresseApi adresseApi;

    public DefaultController(CotisationApi cotisationApi, AdresseApi adresseApi) {
        this.cotisationApi = cotisationApi;
        this.adresseApi = adresseApi;
    }

    @GetMapping("/details/{nom}")
    public InfoMembreRexpaf getInfoMembre(@PathVariable String nom) {

        Cotisation cotisation = this.cotisationApi.detailsCotisations(nom);
        Adresse adresse = this.adresseApi.detailsAdresse(nom);


        InfoMembreRexpaf infoMembreRexpaf = new InfoMembreRexpaf();

        infoMembreRexpaf.setNom(nom);
        infoMembreRexpaf.setAddresse(adresse.getDetails());
        infoMembreRexpaf.setMontant(cotisation.getMontant());

        return infoMembreRexpaf;
    }
}
