package com.rexpaf.rexpafcotisationsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RexpafCotisationsApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RexpafCotisationsApiApplication.class, args);
    }
}
