package com.rexpaf.rexpafcotisationsapi;

import com.rexpaf.rexpafcotisationsapi.api.RecuperationCotisationParNomApiDelegate;
import com.rexpaf.rexpafcotisationsapi.model.Cotisation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class RexpafCotisationController implements RecuperationCotisationParNomApiDelegate {

    @Override
    public ResponseEntity<Cotisation> detailsCotisations(String nom) {
        return new ResponseEntity<>(new Cotisation().nom(nom).montant("500"), HttpStatus.OK);
    }
}
